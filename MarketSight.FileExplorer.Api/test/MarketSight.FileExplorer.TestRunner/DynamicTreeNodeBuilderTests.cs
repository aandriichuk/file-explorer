﻿namespace MarketSight.FileExplorer.TestRunner
{
    using BLL.TreeBuilder;

    using NUnit.Framework;

    [TestFixture]
    [Category("Unit")]
    [Category("Dynamic tree node builder unit tests")]
    public class DynamicTreeNodeBuilderTests
    {
        [Test]
        public void BuildDriveShouldWorkAndCanContainFolders()
        {
            var builder = new DynamicTreeNodeBuilder();
            builder.BuildTopLevelNodes();
            var nodes = builder.GetNodes().ToList();

            nodes.ForEach(
                p =>
                    {
                        Assert.GreaterOrEqual(p.Items.Count, 0);
                    });
        }

        [Test]
        public void BuildFolderShouldWork()
        {
            var builder = new DynamicTreeNodeBuilder();
            builder.BuildDirectoryNodes(@"C:\Projects");
            var nodes = builder.GetNodes().ToList();

            Assert.GreaterOrEqual(nodes.Count, 0);
        }
    }
}
