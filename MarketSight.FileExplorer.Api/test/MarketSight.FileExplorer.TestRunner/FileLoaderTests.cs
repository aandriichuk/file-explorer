﻿namespace MarketSight.FileExplorer.BLL.Test
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    using MarketSight.FileExplorer.BLL.Loader;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Internal;
    using Microsoft.Extensions.Primitives;

    using NUnit.Framework;
    using System.Net.Http.Headers;

    [TestFixture]
    [Category("Unit")]
    [Category("File processing unit tests")]
    public class FileLoaderTests
    {
        private DirectoryInfo testDir;

        private FileInfo testFile;

        [OneTimeSetUp]
        public void Init()
        {
            // Create test directory
            var currentDir = Directory.GetCurrentDirectory();
            this.testDir = Directory.CreateDirectory(string.Concat(currentDir, "\\TestDir"));
            string testFilePath = string.Concat(this.testDir.FullName, "\\TestFile.txt");
            this.testFile = new FileInfo(testFilePath);

            if (!File.Exists(this.testFile.FullName))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(this.testFile.FullName))
                {
                    sw.WriteLine("Hello");
                    sw.WriteLine("And");
                    sw.WriteLine("Welcome");
                }
            }
        }

        [OneTimeTearDown]
        public void Cleanup()
        {
            Directory.Delete(this.testDir.FullName, true);
        }

        [Test]
        public void DownloadFileShouldReturnResult()
        {
            // ARRANGE
            var fileLoader = new FileLoader();

            // ACT
            fileLoader.DownloadFile(this.testFile.FullName);
            var processedFiles = fileLoader.operationResult.processedFiles;

            // ASSERT
            Assert.IsNotNull(processedFiles);
            Assert.NotZero(processedFiles.Count);
            Assert.NotZero(processedFiles[0].Content.Length);
            Assert.Contains(fileLoader.operationResult.Message, new List<string> { "1 file / 21 bytes downloaded successfully!" });
        }

        [Test]
        public void UploadFileShouldWork()
        {
            // ARRANGE
            var fileLoader = new FileLoader();
            var expectedFileName = "Copy of " + this.testFile.Name;

            // ACT
            var file = File.ReadAllBytes(this.testFile.FullName);
            var fileCollection = new Dictionary<string, byte[]> { { this.testFile.Name, file } };
            fileLoader.UploadFiles(fileCollection, this.testDir.FullName);
            var processedFiles = fileLoader.operationResult.processedFiles;

            // ASSERT
            Assert.IsNotNull(processedFiles);
            Assert.NotZero(processedFiles.Count);
            Assert.NotZero(processedFiles[0].Content.Length);
            Assert.Contains(fileLoader.operationResult.Message, new List<string> { "1 file(s) / 21 bytes uploaded successfully!" });
            Assert.AreEqual(expectedFileName, processedFiles[0].FileDetails.Name);
        }
    }
}
