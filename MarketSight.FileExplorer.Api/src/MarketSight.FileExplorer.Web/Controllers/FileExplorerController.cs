using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MarketSight.FileExplorer.Web.Controllers
{
    using System.IO;
    using System.Net;
    using System.Net.Http;

    using MarketSight.FileExplorer.BLL.Commands;
    using MarketSight.FileExplorer.BLL.Loader;
    using MarketSight.FileExplorer.BLL.TreeBuilder;

    using Microsoft.AspNetCore.Hosting.Internal;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;

    using Newtonsoft.Json.Linq;
    using System.Net.Http.Headers;

    using Microsoft.AspNetCore.StaticFiles;
    using Microsoft.CodeAnalysis.CSharp.Syntax;

    [Route("api/[controller]")]
    public class FileExplorerController : Controller
    {

        private readonly ITreeBuilder treeBuilder;

        private readonly ILogger logger;

        private readonly ILoader fileLoader;

        public FileExplorerController(ITreeBuilder treeBuilder, ILogger<FileExplorerController> logger, ILoader fileLoader)
        {
            this.treeBuilder = treeBuilder;
            this.logger = logger;
            this.fileLoader = fileLoader;
        }

        [HttpGet]
        [Route("rootfolders")]
        public IActionResult GetRootFolders()
        {
            this.treeBuilder.BuildTopLevelNodes();
            var nodes = this.treeBuilder.GetNodes().ToList();
            return this.Ok(nodes);
        }

        [HttpGet]
        [Route("nodesbypath")]
        public async Task<IActionResult> GetNodesByPath(string path)
        {
            return await Task.Run(
                       () =>
                           {
                               List<TreeNode> nodes = null;
                               try
                               {
                                   this.treeBuilder.BuildDirectoryNodes(path);
                                   this.treeBuilder.BuildFileNodes(path);
                                   nodes = this.treeBuilder.GetNodes().ToList();
                               }
                               catch (Exception e)
                               {
                                   this.logger.LogError(100, e, "GetNodesByPath. Path {0}", path);
                                   return this.Ok(new { message = "Internal server error", status = StatusCodes.Status500InternalServerError });
                               }

                               return this.Ok(nodes);
                           });
        }

        [HttpGet]
        [Route("foldertotalsize")]
        public async Task<IActionResult> GetFolderTotalSize(string path)
        {
            return await Task.Run(
                       () =>
                           {
                               long dirTotalSize = 0;

                               try
                               {
                                   dirTotalSize = this.treeBuilder.GetDirectoryTotalSize(path);
                               }
                               catch (Exception e)
                               {
                                   this.logger.LogError(100, e, "GetFolderTotalSize. Folder {0}", path);
                                   return this.Ok(new { message = "Internal server error", status = StatusCodes.Status500InternalServerError });
                               }

                               return this.Ok(dirTotalSize);
                           });
        }

        [HttpPost]
        [Route("downloadfile")]
        public IActionResult DownloadFile(CommandArgs args)
        {
            try
            {
                this.fileLoader.DownloadFile(args.TargetSource);
            }
            catch (Exception e)
            {
                this.logger.LogError(100, e, "DownloadFile. File {0}", args.TargetSource);
                return this.Ok(new { message = "Internal server error", status = StatusCodes.Status500InternalServerError });
            }
            
            var fileContent = this.fileLoader.operationResult.processedFiles[0].Content;
            var fileName = this.fileLoader.operationResult.processedFiles[0].FileDetails.Name;
            string contentType;
            new FileExtensionContentTypeProvider().TryGetContentType(args.TargetSource, out contentType);
            var result = new FileStreamResult(new MemoryStream(fileContent), contentType ?? "application/octet-stream")
                             {
                                 FileDownloadName = fileName
                             };

            return result;
        }

        [HttpPost]
        [Route("uploadfile")]
        public IActionResult UploadFile(IFormFile file, string path)
        {
            try
            {
                Dictionary<string, byte[]> filesToSave = new Dictionary<string, byte[]>();
                using (var stream = new MemoryStream())
                {
                    file.CopyTo(stream);
                    filesToSave.Add(file.FileName, stream.ToArray());
                }

                this.fileLoader.UploadFiles(filesToSave, path);
            }
            catch (Exception e)
            {
                this.logger.LogError(100, e, "UploadFile. File {0}, Path {1}", file.FileName, path);
                return this.Ok(new { message = "Internal server error", status = StatusCodes.Status500InternalServerError });
            }

            var loadedFile = this.fileLoader.operationResult.processedFiles[0].FileDetails;
            var node = new TreeNode
                           {
                               Id = loadedFile.FullName,
                               PID = loadedFile.DirectoryName,
                               Text = loadedFile.Name,
                               Icon = "computer-icons empty-doc",
                               Path = loadedFile.FullName,
                               Size = loadedFile.Length
                           };
            return this.Ok(new { node = node, message = "File uploaded!", status = StatusCodes.Status200OK });
        }

        [HttpPost]
        [Route("deletefile")]
        public IActionResult DeleteFile(CommandArgs args)
        {
            try
            {
                var deleteFileCommand = new DeleteFileCommand(args.TargetSource);
                deleteFileCommand.Execute();
            }
            catch (Exception e)
            {
                this.logger.LogError(100, e, "DeleteFile. Path {0}", args.TargetSource);
                return this.Ok(new { message = "Internal server error", status = StatusCodes.Status500InternalServerError });
            }

            return this.Ok(new { message = "File deleted!", status = StatusCodes.Status200OK });
        }

        [HttpPost]
        [Route("deletefolder")]
        public IActionResult DeleteFolder(CommandArgs args)
        {
            try
            {
                var deleteDirCommand = new DeleteDirCommand(args.TargetSource);
                deleteDirCommand.Execute();
            }
            catch (Exception e)
            {
                this.logger.LogError(100, e, "DeleteFile. Path {0}", args.TargetSource);
                return this.Ok(new { message = "Internal server error", status = StatusCodes.Status500InternalServerError });
            }

            return this.Ok(new { message = "Folder deleted!", status = StatusCodes.Status200OK });
        }

        [HttpPost]
        [Route("createsubfolder")]
        public IActionResult CreateSubFolder(CommandArgs args)
        {
            try
            {
                var createSubDirCommand = new CreateSubDirCommand(args.DestSource, args.NewName);
                createSubDirCommand.Execute();
            }
            catch (Exception e)
            {
                this.logger.LogError(100, e, "CreateSubFolder. DestSource {0}. SubFolderName {1}", args.DestSource, args.NewName);
                return this.Ok(new { message = "Internal server error", status = StatusCodes.Status500InternalServerError });
            }

            return this.Ok(new { message = "Sub folder created!", status = StatusCodes.Status200OK });
        }

        [HttpPost]
        [Route("renamefile")]
        public IActionResult RenameFile(CommandArgs args)
        {
            try
            {
                var renameFileCommand = new RenameFileCommand(args.TargetSource, args.NewName);
                renameFileCommand.Execute();
            }
            catch (Exception e)
            {
                this.logger.LogError(100, e, "RenameFile. Path {0}, new name {1}", args.TargetSource, args.NewName);
                return this.Ok(new { message = $"Internal server error {e.Message}", status = StatusCodes.Status500InternalServerError });
            }

            return this.Ok(new { message = "File renamed!", status = StatusCodes.Status200OK });
        }

        [HttpPost]
        [Route("renamefolder")]
        public IActionResult RenameFolder(CommandArgs args)
        {
            try
            {
                var renameDirCommand = new RenameDirCommand(args.TargetSource, args.NewName);
                renameDirCommand.Execute();
            }
            catch (Exception e)
            {
                this.logger.LogError(100, e, "RenameFolder. Path {0}, new name {1}", args.TargetSource, args.NewName);
                return this.Ok(new { message = $"Internal server error {e.Message}", status = StatusCodes.Status500InternalServerError });
            }

            return this.Ok(new { message = "Folder renamed!", status = StatusCodes.Status200OK });
        }

        [HttpPost]
        [Route("copyfile")]
        public IActionResult CopyFile(CommandArgs args)
        {
            try
            {
                var copyFileCommand = new CopyFileCommand(args.TargetSource, args.DestSource);
                copyFileCommand.Execute();
            }
            catch (Exception e)
            {
                this.logger.LogError(100, e, "CopyFolder. Path {0}, CopyToPath {1}", args.TargetSource, args.DestSource);
                return this.Ok(new { message = "Internal server error", status = StatusCodes.Status500InternalServerError });
            }

            return this.Ok(new { message = "File copied!", status = StatusCodes.Status200OK });
        }

        [HttpPost]
        [Route("copyfolder")]
        public IActionResult CopyFolder(CommandArgs args)
        {
            try
            {
                var copyDirCommand = new CopyDirCommand(args.TargetSource, args.DestSource, true);
                copyDirCommand.Execute();
            }
            catch (Exception e)
            {
                this.logger.LogError(100, e, "CopyFolder. Path {0}, CopyToPath {1}", args.TargetSource, args.DestSource);
                return this.Ok(new { message = "Internal server error", status = StatusCodes.Status500InternalServerError });
            }

            return this.Ok(new { message = "Folder copied!", status = StatusCodes.Status200OK });
        }
    }
}
