﻿import { Injectable } from '@angular/core'
import { Headers, Http, RequestOptions, URLSearchParams, ResponseContentType  } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class FileExplorerService {

    private fileExplorerUrl = 'http://localhost:49175/api/fileexplorer';
    private headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });

    constructor(private http: Http) { }

    getTreeNodes(): Promise<any[]>  {
        return this.http.get(this.fileExplorerUrl + '/rootfolders')
            .toPromise()
            .then(response => response.json() as any[])
            .catch(this.handleError);
    }

    getTreeNodesByPath(path: string): Promise<any[]> {
        return this.http.get(this.fileExplorerUrl + '/nodesbypath?path=' + path)
            .toPromise()
            .then(response => response.json() as any[])
            .catch(this.handleError);
    }

    getFolderTotalSize(path: string): Promise<any[]> {
        return this.http.get(this.fileExplorerUrl + '/foldertotalsize?path=' + path)
            .toPromise()
            .then(response => response.json() as any[])
            .catch(this.handleError);
    }

    deleteFile(path: string): Promise<any> {
        let params = new URLSearchParams();
        params.set('TargetSource', path);

        return this.http.post(this.fileExplorerUrl + '/deletefile', params.toString(), { headers: this.headers })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteFolder(path: string): Promise<any> {
        let params = new URLSearchParams();
        params.set('TargetSource', path);

        return this.http.post(this.fileExplorerUrl + '/deletefolder', params.toString(), { headers: this.headers })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    createSubFolder(path: string, newFolderName: string) {
        let params = new URLSearchParams();
        params.set('DestSource', path);
        params.set('NewName', newFolderName);

        return this.http.post(this.fileExplorerUrl + '/createsubfolder', params.toString(), { headers: this.headers })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    renameFile(path: string, newName: string): Promise<any> {
        let params = new URLSearchParams();
        params.set('TargetSource', path);
        params.set('NewName', newName);

        return this.http.post(this.fileExplorerUrl + '/renamefile', params.toString(), { headers: this.headers })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    renameFolder(path: string, newName: string): Promise<any> {
        let params = new URLSearchParams();
        params.set('TargetSource', path);
        params.set('NewName', newName);

        return this.http.post(this.fileExplorerUrl + '/renamefolder', params.toString(), { headers: this.headers })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    copyFile(path: string, copyToPath: string): Promise<any> {
        let params = new URLSearchParams();
        params.set('TargetSource', path);
        params.set('DestSource', copyToPath);

        return this.http.post(this.fileExplorerUrl + '/copyfile', params.toString(), { headers: this.headers })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    copyFolder(path: string, copyToPath: string): Promise<any> {
        let params = new URLSearchParams();
        params.set('TargetSource', path);
        params.set('DestSource', copyToPath);

        return this.http.post(this.fileExplorerUrl + '/copyfolder', params.toString(), { headers: this.headers })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    uploadFile(path: string, file): Promise<any> {
        let fileToUpload = new FormData();
        fileToUpload.append("file", file);
        fileToUpload.append("path", path);

        return this.http.post(this.fileExplorerUrl + '/uploadfile', fileToUpload)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    downloadFile(path: string, fileName: string): Promise<any> {
        let params = new URLSearchParams();
        params.set('TargetSource', path);

        return this.http.post(this.fileExplorerUrl + '/downloadfile', params.toString(), { headers: this.headers, responseType: ResponseContentType.Blob })
            .toPromise()
            .then(response => this.processFile(response, fileName))
            .catch(this.handleError);
    }

    private processFile(response, fileName): Promise<any> {
        let blob = new Blob([response._body]);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.attributes['style'] = "display: none";

        let fileUrl = window.URL.createObjectURL(blob);
        a.href = fileUrl;
        a.download = fileName;
        a.click();
        document.body.removeChild(a);

        return response;
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}