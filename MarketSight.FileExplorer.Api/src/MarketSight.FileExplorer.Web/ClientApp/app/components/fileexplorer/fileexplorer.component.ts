import { Component, ViewEncapsulation, ViewChild, ViewContainerRef } from '@angular/core';
import { IntegralUITreeView } from 'integralui-web/components/integralui.treeview';
import { FileExplorerService } from '../file-explorer.service';

@Component({
    selector: 'treeview',
    template: require('./fileexplorer.component.html'),
    styles: [require('./fileexplorer.component.css')],
    encapsulation: ViewEncapsulation.None
})
export class TreeViewComponent {
    // Get a reference of application view
    @ViewChild('application', { read: ViewContainerRef }) applicationRef: ViewContainerRef;
    @ViewChild('treeview') treeview: IntegralUITreeView;

    // An Array object that holds all item objects shown in TreeView
    // It is set as a list of any custom objects, you can use any custom fields and data bind them with TreeView using its properties
    private items: any[];

    // Editor settings
    private isEditActive: boolean = false;
    private editItem = null;
    private originalText: string = '';
    private editorFocused: boolean = false;
    private hoverItem = null;

    constructor(private fileExplorerService: FileExplorerService) {
        this.items = [];
        this.fileExplorerService.getTreeNodes()
            .then(nodes => this.showItems(nodes))
            .then(() => {
                this.treeview.beforeExpand.subscribe(event => this.beforeExpand(event)),
                    this.treeview.afterCollapse.subscribe(event => this.afterCollapse(event)) });
    }

    showItems(nodes) {
        this.sortByText(nodes);        
        for (let entry of nodes) {
            if (entry.hasChildren === true) {
                entry.items = [];
                entry.items.push({ ext: '' });
            }

            this.treeview.insertItemAt(entry, 0);
        }
    }

    canChange(item){
        return (item.icon.indexOf('folder') > -1 || item.icon.indexOf('doc') > -1);
    }

    canCreateSubfolder(item) {
        return (item.icon.indexOf('folder') > -1 || item.icon.indexOf('disk') > -1);
    }

    canUpload(item) {
        return (item.icon.indexOf('folder') > -1);
    }

    canDownload(item) {
        return (item.icon.indexOf('doc') > -1);
    }

    createSubfolder(item) {
        let newFolderName = "New folder";
        let folderNum = this.getItemNumber(item.items, newFolderName, 'folder');
        if (folderNum > 0) newFolderName = newFolderName + ' (' + folderNum + ')';

        let newItem = {
            id: item.id + '\\' + newFolderName,
            pid: item.id,
            text: newFolderName,
            icon: 'computer-icons folder',
            expanded: false,
            path: item.id + '\\' + newFolderName,
            size: 0.0,
            parent: item,
            sortOrder: 2,
            items: []            
        }

        let path = item.parent != null ? item.parent.path + '\\' + item.text : item.path;
        this.fileExplorerService.createSubFolder(path, newFolderName).then(response => {
            if (response.status === 200) {
                item.items.push(newItem);
                alert(response.message);
            }
        });
    }

    getItemNumber(nodes, newFolderName, itemType) {
        let num = 0;
        for (var i = 0; i < nodes.length; i++) {
            let currentNode = nodes[i];
            if (currentNode.icon.indexOf(itemType) <= 0)
                continue;

            if (currentNode.text.toLowerCase().indexOf(newFolderName.toLowerCase()) >-1)
                num++;
        }

        return num;
    }

    downloadFile(item) {
        let path = item.parent != null ? item.parent.path + '\\' + item.text : item.path;
        this.fileExplorerService.downloadFile(path, item.text).then(response => {
            alert(response.statusText);
        });
    }

    uploadFile(item) {
        var i = document.createElement('INPUT');
        i.setAttribute('id', 'uploader');
        i.setAttribute('type', 'file');
        i.setAttribute('style', 'display: none');
        document.body.appendChild(i);

        i.onchange = () => {
            let uploader = <HTMLInputElement>i;
            let file = uploader.files[0];
            let path = item.parent != null ? item.parent.path + '\\' + item.text : item.path;
            this.fileExplorerService.uploadFile(path, file).then(response => {
                document.body.removeChild(uploader);
                response.node.parent = item;
                if (item.items == null) item.items = [];
                item.items.push(response.node);
                alert(response.message);

                return;
            });
        }

        i.click();
    }

    deleteItem(item) {
        if (item.icon.indexOf('folder') > -1)
            this.deleteFolder(item);
        if (item.icon.indexOf('doc') > -1)
            this.deleteFile(item);
    }

    renameItem(item) {
        if (item.text === this.originalText) return;

        if (item.icon.indexOf('folder') > -1)
            this.renameFolder(item);
        if (item.icon.indexOf('doc') > -1)
            this.renameFile(item);
    }

    copyItem(item) {
        if (item.icon.indexOf('folder') > -1)
            this.copyFolder(item);
        if (item.icon.indexOf('doc') > -1)
            this.copyFile(item);
    }

    deleteFile(item) {
        this.fileExplorerService.deleteFile(item.path).then(response => {
            if (response.status === 200) {
                this.treeview.removeItem(item);
            }
            alert(response.message);
        });
    }

    deleteFolder(item) {
        let path = item.parent != null ? item.parent.path + '\\' + item.text : item.path;
        this.fileExplorerService.deleteFolder(path).then(response => {
            if (response.status === 200) {
                this.treeview.removeItem(item);
            }
            alert(response.message);
        });
    }

    renameFile(item) {
        let path = item.parent != null ? item.parent.path + '\\' + this.originalText : item.path;
        this.fileExplorerService.renameFile(path, item.text).then(response => {
            if (response.status === 500) {
                item.text = this.originalText;
            }
            else if (response.status === 200) {
                item.id = item.parent.id + '\\' + item.text;
                item.path = item.parent.id + '\\' + item.text;
            }
            alert(response.message);
        });
    }

    renameFolder(item) {
        let path = item.parent != null ? item.parent.path + '\\' + this.originalText : item.path;
        this.fileExplorerService.renameFolder(path, item.text).then(response => {
            if (response.status === 500) {
                item.text = this.originalText;
            }
            else if (response.status === 200) {
                item.id = item.parent.id + '\\' + item.text;
                item.path = item.parent.id + '\\' + item.text;
            }
            alert(response.message);
        });
    }

    copyFile(item) {
        var copiedItemText = 'Copy of ' + item.text;
        let folderNum = this.getItemNumber(item.parent.items, copiedItemText, 'doc');
        var numStr = ' (' + folderNum + ')'; 
        var position = copiedItemText.indexOf('.');

        if (folderNum > 0) copiedItemText = [copiedItemText.slice(0, position), numStr, copiedItemText.slice(position)].join('');

        let path = item.parent != null ? item.parent.path + '\\' + item.text : item.path;
        this.fileExplorerService.copyFile(path, item.parent.path + '\\' + copiedItemText).then(response => {
            if (response.status === 200) {
                var copyItem: any = Object.assign({}, item);
                copyItem.parent = Object.assign({}, item.parent);
                copyItem.id = item.parent.id + '\\' + copiedItemText;
                copyItem.path = item.parent.id + '\\' + copiedItemText;
                copyItem.text = copiedItemText;
                //copyItem.items = this.deepClone(item.items);
                item.parent.items.push(copyItem);
            }
            alert(response.message);
        });
    }

    copyFolder(item) {
        let copiedItemText = 'Copy of ' + item.text;
        let folderNum = this.getItemNumber(item.parent.items, copiedItemText, 'folder');
        if (folderNum > 0) copiedItemText = copiedItemText + ' (' + folderNum + ')';
        let path = item.parent != null ? item.parent.path + '\\' + item.text : item.path;
        this.fileExplorerService.copyFolder(path, item.parent.path + '\\' + copiedItemText).then(response => {
            if (response.status === 200) {
                var copyItem: any = Object.assign({}, item);
                copyItem.parent = Object.assign({}, item.parent);
                copyItem.id = item.parent.id + '\\' + copiedItemText;
                copyItem.path = item.parent.id + '\\' + copiedItemText;
                copyItem.text = copiedItemText;
                copyItem.items = this.deepClone(copyItem, item.items);
                item.parent.items.push(copyItem);
            }
            alert(response.message);
        });
    }

    deepClone(parent, items) {
        let itemsClone = [];
        for (let entry of items) {
            var copyItem: any = Object.assign({}, entry);
            copyItem.parent = Object.assign({}, entry.parent);
            copyItem.id = parent.id + '\\' + entry.text;
            copyItem.path = parent.path + '\\' + entry.text;
            copyItem.text = entry.text;
            console.log(copyItem);
            if (entry.items && entry.items.length > 0)
                copyItem.items = this.deepClone(copyItem, entry.items);

            itemsClone.push(copyItem);
        }

        return itemsClone;
    }

    showEditor(item) {
        this.originalText = item.text;
        this.isEditActive = true;
        this.editItem = item;
        this.editorFocused = true;
    }

    closeEditor() {
        this.editItem = null;
        this.originalText = '';
        this.editorFocused = false;
    }

    editorKeyDown(e) {
        if (this.editItem) {
            switch (e.keyCode) {
                case 13: { // ENTER
                    this.renameItem(this.editItem);
                    this.closeEditor();
                    break;
                }
            case 27: // ESCAPE
                this.editItem.text = this.originalText;
                this.closeEditor();
                break;
            }
        }
    }

    editorLostFocus() {
        if (this.editItem)
            this.editItem.text = this.originalText;

        this.closeEditor();
    }

    beforeExpand(event) {
        let item = event.item;
        if (!item) return;

        while (item.items.length) { item.items.pop(); }
        this.fileExplorerService.getTreeNodesByPath(item.path).then(nodes => {
            for (let node of nodes) {
                node.parent = item;
                item.items.push(node);

                for (let child of item.items) {
                    if (child.hasChildren === true) {
                        child.items = [];
                        child.items.push({ ext: '' });
                    }
                }
            }

            this.sortByOrder(item.items);
        });
    }

    afterCollapse(event) {
        let item = event.item;
        if (!item) return;

        while (item.items.length) { item.items.pop(); }

        if (item.hasChildren === true) {
            item.items = [];
            item.items.push({ text: '' });
        }
    }

    itemClick(item) {
        this.showFolderSize(item);
    }

    showFolderSize(item) { 
        if (item.icon.indexOf('folder') > -1) {
            let path = item.parent != null ? item.parent.path + '\\' + item.text : item.path;
            console.log(path);
            this.fileExplorerService.getFolderTotalSize(path).then(size => {
                item.size = size;
            });
        }
    }

    sortByOrder(items) {
        items.sort((node1, node2) => this.compare(node1.sortOrder, node2.sortOrder));    
    }

    sortByText(items) {
        items.sort((node1, node2) => this.compare(node2.text, node1.text));
    }

    compare(a, b) {
        if (a < b) {
            return -1;
        }

        if (a > b) {
            return 1;
        }

        // a must be equal to b
        return 0;
    }
}

