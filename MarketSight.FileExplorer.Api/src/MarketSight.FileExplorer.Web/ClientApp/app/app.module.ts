import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UniversalModule } from 'angular2-universal';
import { FormsModule } from '@angular/forms';
import { IntegralUIModule } from 'integralui-web/integralui.module';
import { AppComponent } from './components/app/app.component'
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { TreeViewComponent } from './components/fileexplorer/fileexplorer.component';
import { FileExplorerService } from './components/file-explorer.service';

@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        TreeViewComponent
    ],
    providers: [
        FileExplorerService
    ],
    imports: [
        UniversalModule, // Must be first import. This automatically imports BrowserModule, HttpModule, and JsonpModule too.
        FormsModule,
        IntegralUIModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'file-explorer', component: TreeViewComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ]
})
export class AppModule {
}
