﻿namespace MarketSight.FileExplorer.BLL.Commands
{
    using System.Collections.Generic;

    using Loader;

    using Microsoft.AspNetCore.Http;

    public class UploadToDirCommand : Command
    {
        private readonly Dictionary<string, byte[]> files;

        private readonly string dirPath;

        private readonly ILoader fileUploader;

        public UploadToDirCommand(ILoader fileUploader, Dictionary<string, byte[]> files, string dirPath)
        {
            this.files = files;
            this.dirPath = dirPath;
            this.fileUploader = fileUploader;
        }

        public override void Execute()
        {
            this.fileUploader.UploadFiles(this.files, this.dirPath);
        }
    }
}
