﻿namespace MarketSight.FileExplorer.BLL.Commands
{
    using System;
    using System.IO;

    public class DeleteDirCommand : Command
    {
        private readonly string dirPath;

        public DeleteDirCommand(string dirPath)
        {
            this.dirPath = dirPath;
        }

        public override void Execute()
        {
            Directory.Delete(this.dirPath, true);

            var directoryExists = Directory.Exists(this.dirPath);
            if (directoryExists)
            {
                throw new Exception($"Cannot delete directory {this.dirPath}"); 
            }
        }
    }
}
