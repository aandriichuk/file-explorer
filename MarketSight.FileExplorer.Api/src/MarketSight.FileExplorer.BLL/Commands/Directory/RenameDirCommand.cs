﻿namespace MarketSight.FileExplorer.BLL.Commands
{
    using System.IO;

    public class RenameDirCommand : Command
    {
        private readonly string dirPath;

        private readonly string newDirName;

        public RenameDirCommand(string dirPath, string newDirName)
        {
            this.dirPath = dirPath;
            this.newDirName = newDirName;
        }

        public override void Execute()
        {
            var dirToRename = new DirectoryInfo(this.dirPath);
            if (dirToRename.Parent != null)
            {
                string path = Path.Combine(dirToRename.Parent.FullName, this.newDirName);
                dirToRename.MoveTo(path);
            }
        }
    }
}
