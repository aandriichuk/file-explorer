﻿namespace MarketSight.FileExplorer.BLL.Commands
{
    using System.IO;

    public class CreateSubDirCommand : Command
    {
        private readonly string parentDirPath;

        private readonly string subDirName;

        public CreateSubDirCommand(string parentDirPath, string subDirName)
        {
            this.parentDirPath = parentDirPath;
            this.subDirName = subDirName;
        }

        public override void Execute()
        {
            // Create a reference to a parent directory.
            var parentDir = new DirectoryInfo(this.parentDirPath);

            var subDir = new DirectoryInfo(string.Concat(this.parentDirPath, "\\", this.subDirName));

            // Create the directory only if it does not already exist.
            if (parentDir.Exists && !subDir.Exists)
            {
                // Create a subdirectory in the directory just created.
                subDir = parentDir.CreateSubdirectory(this.subDirName);
            }

            if (!subDir.Exists)
            {
                throw new DirectoryNotFoundException(
                    $"Sub directory does not exist or could not be found: {this.subDirName}");
            }
        }
    }
}
