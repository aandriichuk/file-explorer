﻿namespace MarketSight.FileExplorer.BLL.Commands
{
    using System.IO;

    public class CopyDirCommand : Command
    {
        private readonly string sourceDirName;

        private readonly string destDirName;

        private readonly bool copySubDirs;

        public CopyDirCommand(string sourceDirName, string destDirName, bool copySubDirs)
        {
            this.sourceDirName = sourceDirName;
            this.destDirName = destDirName;
            this.copySubDirs = copySubDirs;
        }

        public override void Execute()
        {
            this.DirectoryCopy(this.sourceDirName, this.destDirName, this.copySubDirs);
        }

        private void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            var dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException($"Source directory does not exist or could not be found: {sourceDirName}");
            }

            var dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            var files = dir.GetFiles();
            foreach (var file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (var subdir in dirs)
                {
                    var temppath = Path.Combine(destDirName, subdir.Name);
                    this.DirectoryCopy(subdir.FullName, temppath, true);
                }
            }
        }
    }
}
