﻿namespace MarketSight.FileExplorer.BLL.Commands
{
    public abstract class Command
    {
        public abstract void Execute();
    }
}
