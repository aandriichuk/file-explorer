﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketSight.FileExplorer.BLL.Commands
{
    public class CommandArgs
    {
        public string TargetSource { get; set; }

        public string DestSource { get; set; }

        public string NewName { get; set; }
    }
}
