﻿namespace MarketSight.FileExplorer.BLL.Commands
{
    using System.IO;

    public class RenameFileCommand : Command
    {
        private readonly string filePath;

        private readonly string newFileName;

        public RenameFileCommand(string filePath, string newFileName)
        {
            this.filePath = filePath;
            this.newFileName = newFileName;
        }

        public override void Execute()
        {
            var fileInfo = new FileInfo(this.filePath);
            if (fileInfo.Directory != null)
            {
                string path = Path.Combine(fileInfo.Directory.FullName, this.newFileName);
                fileInfo.MoveTo(path);
            }
        }
    }
}
