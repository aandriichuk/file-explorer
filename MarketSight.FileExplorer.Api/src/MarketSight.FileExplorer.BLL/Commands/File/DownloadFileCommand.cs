﻿namespace MarketSight.FileExplorer.BLL.Commands
{
    using MarketSight.FileExplorer.BLL.Loader;

    public class DownloadFileCommand : Command
    {
        private readonly string filePath;

        private readonly ILoader fileDownloader;

        public DownloadFileCommand(ILoader fileDownloader, string filePath)
        {
            this.filePath = filePath;
            this.fileDownloader = fileDownloader;
        }

        public override void Execute()
        {
            this.fileDownloader.DownloadFile(this.filePath);
        }
    }
}
