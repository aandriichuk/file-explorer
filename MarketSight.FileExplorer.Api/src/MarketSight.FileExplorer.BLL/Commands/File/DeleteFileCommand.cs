﻿namespace MarketSight.FileExplorer.BLL.Commands
{
    using System.IO;

    public class DeleteFileCommand : Command
    {
        private readonly string destFile;

        public DeleteFileCommand(string destFile)
        {
            this.destFile = destFile;
        }

        public override void Execute()
        {
            File.Delete(this.destFile);
        }
    }
}
