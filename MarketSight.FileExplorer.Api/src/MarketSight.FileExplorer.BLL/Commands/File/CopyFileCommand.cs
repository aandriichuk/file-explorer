﻿using System.IO;

namespace MarketSight.FileExplorer.BLL.Commands
{
    public class CopyFileCommand : Command
    {
        private readonly string sourceFile;

        private readonly string destFile;

        public CopyFileCommand(string sourceFile, string destFile)
        {
            this.sourceFile = sourceFile;
            this.destFile = destFile;
        }

        public override void Execute()
        {
            File.Copy(this.sourceFile, this.destFile, true);
        }
    }
}
