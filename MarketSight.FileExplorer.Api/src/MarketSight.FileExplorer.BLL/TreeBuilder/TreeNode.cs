﻿using System.Collections.Generic;

namespace MarketSight.FileExplorer.BLL.TreeBuilder
{
    /// <summary>
    /// Represents tree node in a treeview 
    /// </summary>
    public class TreeNode
    {
        /// <summary>
        /// Gets or sets a unique identifier used to differentiate items from each other
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the identifier of parent item to which an item is child of
        /// </summary>
        public string PID { get; set; }

        /// <summary>
        /// Gets or sets the item label
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets css class for item icon
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Gets or sets item is in expanded or collapsed state
        /// </summary>
        public bool Expanded { get; set; }

        /// <summary>
        /// Gets or sets full path to specific entry
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets size of file or folder
        /// </summary>
        public double Size { get; set; }

        /// <summary>
        /// Gets or sets parent node
        /// </summary>
        public TreeNode Parent { get; set; }

        /// <summary>
        /// Gets or sets order of sorting
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        /// Gets or sets HasChildren sign if node has children
        /// </summary>
        public bool HasChildren { get; set; }

        /// <summary>
        /// Gets or sets child directories and files 
        /// </summary>
        public List<TreeNode> Items { get; set; }
    }
}
