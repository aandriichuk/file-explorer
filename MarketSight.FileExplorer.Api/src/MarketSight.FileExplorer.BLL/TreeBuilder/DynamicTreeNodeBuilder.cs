﻿namespace MarketSight.FileExplorer.BLL.TreeBuilder
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class DynamicTreeNodeBuilder : ITreeBuilder
    {
        private ConcurentTreeNodeCollection nodes = new ConcurentTreeNodeCollection();

        public void BuildTopLevelNodes()
        {
            var drives =
                DriveInfo.GetDrives().Where(d => d.IsReady)
                    .Select(
                        drive =>
                            new TreeNode
                                {
                                    Id = drive.Name.Replace(@"\", string.Empty),
                                    Text = drive.Name,
                                    Icon = "computer-icons disk",
                                    Path = drive.Name.Replace(@"\", string.Empty),
                                    Expanded = false,
                                    Size = drive.TotalSize - drive.TotalFreeSpace,
                                    SortOrder = 1,
                                    HasChildren = drive.TotalSize > 0
                                }).ToList();
            this.nodes.AddRange(drives);
        }

        public void BuildDirectoryNodes(string path)
        {
            var childFolderNodes = this.GetDirectoryNodes(path);
            this.nodes.AddRange(childFolderNodes);
        }

        public void BuildFileNodes(string path)
        {
            var childFileNodes = this.GetFileNodes(path);
            this.nodes.AddRange(childFileNodes);
        }

        public ConcurentTreeNodeCollection GetNodes()
        {
            return this.nodes;
        }

        private List<TreeNode> GetFileNodes(string path)
        {
            DirectoryInfo currentDir = new DirectoryInfo(path);
            FileInfo[] files = currentDir.GetFiles("*", SearchOption.TopDirectoryOnly);
            string[] excludedFiles = { ".dll", ".bin", ".exe", ".lng" };

            var childFileNodes =
                files.Where(
                        f => !f.Attributes.HasFlag(FileAttributes.Hidden) & !f.Attributes.HasFlag(FileAttributes.System) & !excludedFiles.Contains(f.Extension.ToLower()))
                    .Select(
                        p =>
                            new TreeNode()
                            {
                                Id = p.FullName,
                                PID = p.DirectoryName,
                                Text = p.Name,
                                Icon = "computer-icons empty-doc",
                                Path = p.FullName,
                                Expanded = false,
                                Size = p.Length,
                                SortOrder = 3,
                                HasChildren = false
                            })
                    .ToList();

            return childFileNodes;
        }

        private List<TreeNode> GetDirectoryNodes(string path)
        {
            var listNodes = new List<TreeNode>();

            try
            {
                DirectoryInfo currentDir = new DirectoryInfo(path);
                DirectoryInfo[] subdirs = currentDir.GetDirectories("*", SearchOption.TopDirectoryOnly);

                listNodes =
                    subdirs.Where(
                            sb =>
                                !sb.Attributes.HasFlag(FileAttributes.Hidden)
                                & !sb.Attributes.HasFlag(FileAttributes.System))
                        .Select(
                            p =>
                                new TreeNode()
                                    {
                                        Id = p.FullName,
                                        PID = p.Parent?.FullName,
                                        Text = p.Name,
                                        Icon = "computer-icons folder",
                                        Path = p.FullName,
                                        Expanded = false,
                                        Size = 0,
                                        SortOrder = 2,
                                        HasChildren = p.GetFiles("*", SearchOption.TopDirectoryOnly).Any() || p.GetDirectories("*", SearchOption.TopDirectoryOnly).Any()
                                })
                        .ToList();
            }
            catch
            {
                // ignored
            }

            return listNodes;
        }

        public long GetDirectoryTotalSize(string path)
        {
            DirectoryInfo currentDir = new DirectoryInfo(path);
            FileInfo[] files = currentDir.GetFiles("*", SearchOption.AllDirectories);

            long total = 0;

            // First type parameter is the type of the source elements
            // Second type parameter is the type of the thread-local variable (partition subtotal)
            Parallel.ForEach<FileInfo, long>(
                files,
                () => 0,
                (f, loop, subtotal) =>
                    {
                        subtotal += f.Length;
                        return subtotal;
                    },
                (finalResult) => Interlocked.Add(ref total, finalResult));

            return total;
        }
    }
}
