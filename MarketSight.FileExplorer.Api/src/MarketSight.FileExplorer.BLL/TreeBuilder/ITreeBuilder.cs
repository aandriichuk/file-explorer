﻿namespace MarketSight.FileExplorer.BLL.TreeBuilder
{
    public interface ITreeBuilder
    {
        void BuildTopLevelNodes();
        void BuildDirectoryNodes(string path);
        void BuildFileNodes(string path);
        long GetDirectoryTotalSize(string path);

        ConcurentTreeNodeCollection GetNodes();
    }
}
