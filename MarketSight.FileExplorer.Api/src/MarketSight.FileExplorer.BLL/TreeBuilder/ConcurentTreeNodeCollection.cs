﻿namespace MarketSight.FileExplorer.BLL.TreeBuilder
{
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;

    public class ConcurentTreeNodeCollection
    {
        private ConcurrentDictionary<string, TreeNode> nodes = new ConcurrentDictionary<string, TreeNode>();

        public void Add(TreeNode node)
        {
            this.nodes.AddOrUpdate(node.Id, node, (k, v) => node);
        }

        public void AddRange(IEnumerable<TreeNode> nodes)
        {
            foreach (var node in nodes)
            {
                this.Add(node);
            }
        }

        public List<TreeNode> ToList()
        {
            return
                this.nodes.Select(
                    n =>
                        new TreeNode
                        {
                            Id = n.Value.Id,
                            Text = n.Value.Text,
                            Icon = n.Value.Icon,
                            Path = n.Value.Path,
                            Expanded = n.Value.Expanded,
                            Size = n.Value.Size,
                            SortOrder = n.Value.SortOrder,
                            HasChildren = n.Value.HasChildren,
                            Items = n.Value.Items
                        }).ToList();
        }
    }
}
