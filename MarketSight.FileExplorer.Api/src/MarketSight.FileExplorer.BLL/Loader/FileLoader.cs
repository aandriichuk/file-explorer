﻿namespace MarketSight.FileExplorer.BLL.Loader
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http.Headers;

    using Microsoft.AspNetCore.Http;

    public class FileLoader : ILoader
    {
        public FileOperationResult operationResult { get; }

        public FileLoader()
        {
            this.operationResult = new FileOperationResult();
        }

        public void DownloadFile(string filePath)
        {
            var fileInfo = new FileInfo(filePath);
            var content = File.ReadAllBytes(filePath);
            var size = content.Length;
            string message = $"1 file / {size} bytes downloaded successfully!";
            this.operationResult.processedFiles = new List<ProcessedFile>
                                                      {
                                                          new ProcessedFile
                                                              {
                                                                  FileDetails = fileInfo,
                                                                  Content = content
                                                              }
                                                      };
            this.operationResult.Message = message;
        }

        public void UploadFiles(Dictionary<string, byte[]> files, string dirPath)
        {
            long size = 0;
            this.operationResult.processedFiles = new List<ProcessedFile>();
            foreach (var file in files)
            {
                var fileName = string.Concat(dirPath, "\\", file.Key);
                if (File.Exists(fileName))
                {
                    fileName = string.Concat(dirPath, "\\", $"Copy of {file.Key}"); 
                }

                this.operationResult.processedFiles.Add(new ProcessedFile { Content = file.Value, FileDetails = new FileInfo(fileName) });
                size += file.Value.Length;

                using (FileStream fs = File.Create(fileName))
                {
                    fs.Write(file.Value, 0, file.Value.Length);
                    fs.Flush();
                }
            }

            this.operationResult.Message = $"{files.Count} file(s) / {size} bytes uploaded successfully!";
        }
    }
}
