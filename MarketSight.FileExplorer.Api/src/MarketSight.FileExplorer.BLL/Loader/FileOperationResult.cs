﻿using System.Collections.Generic;

namespace MarketSight.FileExplorer.BLL.Loader
{
    public class FileOperationResult
    {
        public List<ProcessedFile> processedFiles { get; set; }

        public string Message { get; set; }
    }
}

