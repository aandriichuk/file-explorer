﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketSight.FileExplorer.BLL.Loader
{
    using System.IO;

    using Microsoft.AspNetCore.Http;

    public interface ILoader
    {
        FileOperationResult operationResult { get; }

        void DownloadFile(string filePath);

        void UploadFiles(Dictionary<string, byte[]> files, string dirPath);
    }
}
