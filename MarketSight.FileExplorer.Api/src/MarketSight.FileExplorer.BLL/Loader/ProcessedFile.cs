﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketSight.FileExplorer.BLL.Loader
{
    using System.IO;

    public class ProcessedFile
    {
        public FileInfo FileDetails { get; set; }

        public byte[] Content { get; set; }
    }
}
